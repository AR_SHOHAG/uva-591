#include <cstdio>
#include <iostream>
using namespace std;

int main()
{
    int n, k, avg=0, ans=0, s=0;

    while(scanf("%d", &n), n>0){
        s++;
        int h[n];
        for(int i=0; i<n; ++i){
            scanf("%d", &h[i]);
            avg+=h[i];
        }
        avg=avg/n;
        for(int i=0; i<n; ++i){
            if(h[i]>avg){
                ans+=h[i]-avg;
            }
        }
        printf("Set #%d\n", s);
        printf("The minimum number of moves is %d.\n", ans);
        printf("\n");
        avg=0, ans=0;
    }

    return 0;
}
